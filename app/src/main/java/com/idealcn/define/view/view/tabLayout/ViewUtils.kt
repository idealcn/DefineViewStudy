package com.idealcn.define.view.view.tabLayout

import android.content.Context
import android.util.TypedValue
import androidx.annotation.Dimension
import androidx.annotation.Dimension.Companion.DP
import kotlin.math.roundToInt

object ViewUtils {
    @JvmStatic
    fun dpToPx(context: Context, @Dimension(unit = DP) dp: Int): Float {
        val resources = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            resources.displayMetrics
        )
    }
    @JvmStatic
    fun lerp(startValue: Int, endValue: Int, fraction: Float): Int {
        return startValue + (fraction * (endValue - startValue)).roundToInt()
    }

}