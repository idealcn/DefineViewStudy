package com.idealcn.define.view

import android.content.Context
import android.content.pm.PackageManager

object AppHelper {

    fun getMetaData(context: Context,key : String) :String?{
        val applicationInfo = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
        val bundle = applicationInfo.metaData
        return bundle.getString(key)
    }

}