package com.idealcn.define.view.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.Scroller;

import androidx.appcompat.widget.LinearLayoutCompat;

public class HorizontalScrollLayout extends LinearLayoutCompat {

    private final Scroller mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());

    public HorizontalScrollLayout(Context context) {
        super(context);
    }

    public HorizontalScrollLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalScrollLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    @Override
//    protected void onLayout(boolean changed, int l, int t, int r, int b) {
//        getChildAt(0).layout(0,0,r,b-t);
//        getChildAt(1).layout(getChildAt(0).getWidth(),0,getChildAt(0).getWidth()+getChildAt(1).getWidth(),b-t);
//    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getChildAt(0).getMeasuredWidth() +
                getChildAt(1).getMeasuredWidth()
                ,Math.max(getChildAt(0).getMeasuredHeight(),getChildAt(1).getMeasuredHeight()));
    }

    private int mStartX;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        System.out.println("当前事件："+ action);
        switch (action){
            case MotionEvent.ACTION_DOWN: {
                mStartX  = (int) event.getRawX();
//                mStartX = (int) event.getX();
                if (!mScroller.isFinished()){
                    mScroller.abortAnimation();
                }
                break;
            }

            case MotionEvent.ACTION_MOVE:{
                System.out.println("child0: x=>"+getChildAt(0).getX()+",left=>"+getChildAt(0).getLeft());
                System.out.println("child1: x=>"+getChildAt(1).getX()+",left=>"+getChildAt(1).getLeft());
                int mOldScrollX = getScrollX();
                int mEndX = (int) event.getRawX();
                int mNewScrollX = mOldScrollX  +   mStartX - mEndX;
                if (mNewScrollX<0){
                    mNewScrollX = 0;
                }
                if (mNewScrollX>getChildAt(1).getMeasuredWidth() ){
                    mNewScrollX = getChildAt(1).getMeasuredWidth();
                }
                System.out.println("ACTION_MOVE: mOldScrollX===>"+mOldScrollX+"mNewScrollX===>"+mNewScrollX);
                scrollTo(mNewScrollX,0);
                //用Scroller实现滑动
//                mScroller.startScroll(mStartX,0,mNewScrollX-mOldScrollX,0);
//                invalidate();
                mStartX = mEndX;
                break;
            }

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP: {
                int scrollX = getScrollX();
                System.out.println("ACTION_UP: scrollX===>"+scrollX);
                int range = getChildAt(1).getWidth();
                    if (scrollX>range/2) {
                        mScroller.startScroll(scrollX,0,range-scrollX,0);
                        invalidate();
                    }else  {
                        mScroller.startScroll(scrollX,0,-scrollX,0);
                        invalidate();
                    }
                break;
            }

            case MotionEvent.ACTION_CANCEL:{
                System.out.println("----------ACTION_CANCEL----------");
                break;
            }

        }
        return true;
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mScroller.computeScrollOffset()){
            scrollTo(mScroller.getCurrX(),mScroller.getCurrY());
            invalidate();
        }
    }
}
