package com.idealcn.define.view.ui.activity

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.idealcn.define.view.R
import com.idealcn.define.view.view.chart.ChartData
import com.idealcn.define.view.view.chart.ChartFactory
import com.idealcn.define.view.view.chart.ChartStyle
import com.idealcn.define.view.view.chart.RectChartView

class ChartActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)
        val chartList = listOf<ChartData>(
                ChartData(100f, Color.RED, ChartStyle.rect),
                ChartData(60f, Color.BLUE, ChartStyle.rect),
                ChartData(75f, Color.GRAY, ChartStyle.rect),
                ChartData(36f, Color.BLACK, ChartStyle.rect),
                ChartData(55f, Color.GREEN, ChartStyle.rect)
                )
        val rectChartView = findViewById<RectChartView>(R.id.rectChartView)
        ChartFactory.get()
                .chartList(chartList)
                .view(rectChartView).create();
    }


}
