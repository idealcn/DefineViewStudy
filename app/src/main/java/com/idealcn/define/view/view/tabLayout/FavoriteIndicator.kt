package com.idealcn.define.view.view.tabLayout

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import com.idealcn.define.view.R

class FavoriteIndicator : View {

    constructor(context: Context?) : this(context,null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs,0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
       setBackgroundResource(R.drawable.shape_mine_favorite_tab_indicator)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(
            resources.getDimensionPixelSize(R.dimen.x60),
            resources.getDimensionPixelSize(R.dimen.x6)
        )
    }
}