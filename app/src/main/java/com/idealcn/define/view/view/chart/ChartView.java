package com.idealcn.define.view.view.chart;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * 日期: 2018/9/22 23:52
 * author: guoning
 * description:
 */
public abstract class ChartView extends View implements IChartView {



    private int chartNumber;
    private ChartStyle chartStyle = ChartStyle.rect;//表格类型
    private List<? extends ChartData> chartDataList = new ArrayList<>();





    public ChartView(Context context) {
        this(context,null);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = 0, h = 0;
        w = MeasureSpec.getSize(widthMeasureSpec);
        h = MeasureSpec.getSize(heightMeasureSpec);
        if (MeasureSpec.getMode(widthMeasureSpec)!=MeasureSpec.EXACTLY){
            w = getMeasuredWidth() - getPaddingRight() - getPaddingLeft();
        }
        if (MeasureSpec.getMode(heightMeasureSpec)!=MeasureSpec.EXACTLY){
            h = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        }
        setMeasuredDimension(w,h);

    }

    public void setData(List<? extends ChartData> chartDataList){
        setChartData(chartDataList);
        invalidate();
    }


    public abstract void setChartData(List<? extends ChartData> chartDataList);
}
