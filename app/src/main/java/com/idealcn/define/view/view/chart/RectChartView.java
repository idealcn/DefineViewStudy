package com.idealcn.define.view.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.idealcn.define.view.utils.DensityUtil;

import java.util.List;

/**
 * 日期: 2018/9/23 15:27
 * author: guoning
 * description:
 */
public class RectChartView extends ChartView {

    private List<? extends ChartData> chartList;

    private Paint chartPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Path chartPath = new Path();


    public RectChartView(Context context) {
        this(context,null);
    }

    public RectChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RectChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        chartPaint.setStyle(Paint.Style.FILL);

    }

    @Override
    public void setChartData(List<? extends ChartData> chartDataList) {
        this.chartList = chartDataList;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int lastLeft = getPaddingLeft()+DensityUtil.dip2px(getContext(),20);

        for (ChartData chart : chartList) {

            chartPath.reset();
            chartPaint.reset();
            chartPaint.setColor(chart.color);
            chartPath.moveTo(lastLeft ,getHeight() - getPaddingBottom());
            chartPath.lineTo(lastLeft,getHeight() - getPaddingBottom() - DensityUtil.dip2px(getContext(),chart.height));
            chartPath.lineTo(lastLeft+DensityUtil.dip2px(getContext(),40),getHeight() - getPaddingBottom() - DensityUtil.dip2px(getContext(),chart.height));
            chartPath.lineTo(lastLeft+DensityUtil.dip2px(getContext(),40),getHeight() - getPaddingBottom());
            canvas.drawPath(chartPath,chartPaint);

            lastLeft += DensityUtil.dip2px(getContext(),40);

        }

    }

    public void getChartList(List<ChartData> chartList){
        this.chartList = chartList;
        invalidate();
    }

}
