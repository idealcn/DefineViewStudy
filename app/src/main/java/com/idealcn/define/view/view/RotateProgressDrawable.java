package com.idealcn.define.view.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.idealcn.define.view.R;


/**
 * 日期: 2018/9/10 10:22
 * author: guoning
 * description:
 */
public class RotateProgressDrawable extends View {

    private Paint mDrawablePaint;

    private static final int inner = 5;

    private boolean firstDraw = true;
    private   int inter = 1;


    public RotateProgressDrawable(Context context) {
        this(context,null);
    }

    public RotateProgressDrawable(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RotateProgressDrawable(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mDrawablePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDrawablePaint.setStrokeWidth(2*getResources().getDisplayMetrics().scaledDensity);
        mDrawablePaint.setStyle(Paint.Style.STROKE);
        mDrawablePaint.setColor(ContextCompat.getColor(context,R.color.colorAccent));



    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.rotate(30);
        canvas.drawLine(getWidth()/2+inner*getResources().getDisplayMetrics().scaledDensity,
                getHeight()/2,getWidth(),0,mDrawablePaint);
//        canvas.rotate(30 * (inter>=12?0:(inter++)));
        canvas.restore();
        firstDraw = false;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();


    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
