package com.idealcn.define.view.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.DebugUtils
import android.view.MotionEvent
import android.view.MotionEvent.*
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Scroller
import com.idealcn.define.view.R
import com.idealcn.define.view.utils.DensityUtil
import java.util.logging.Logger

/**
 *  todo 加个边界约束,避免滑动超出屏幕边界
 */
class MoveView : View {

    private val logger = Logger.getLogger("MoveView")

    var lastX: Float = 0f
    var lastY: Float = 0f

    var defaultText = ""
    var moveBackgroundColor = Color.WHITE
    var textColor: Int = Color.BLACK
    var textSize = 14
    val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)

     var statusBarHeight  : Int

    init {

        textPaint.style = Paint.Style.STROKE
        textPaint.textSize = textSize.toFloat()
        textPaint.color = textColor

    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributes: AttributeSet?) : this(context, attributes, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle) {


        val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.MoveView)
        textSize = typedArray.getDimensionPixelSize(R.styleable.MoveView_move_textSize, textSize)
        defaultText = typedArray.getText(R.styleable.MoveView_move_text).toString()
        textColor = typedArray.getColor(R.styleable.MoveView_move_textColor, textColor)
        moveBackgroundColor = typedArray.getColor(R.styleable.MoveView_move_backgroundColor, moveBackgroundColor)
        typedArray.recycle()
        textPaint.color = textColor
        setBackgroundColor(moveBackgroundColor)
        textPaint.textSize = textSize.toFloat()


        statusBarHeight = DensityUtil.getStatusBarHeight(context)

    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let {
            val fontMetrics = textPaint.fontMetrics

            it.drawText(defaultText, (width - textPaint.measureText(defaultText)) / 2,
                    (height + fontMetrics.descent - fontMetrics.ascent) / 2, textPaint)

        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let {
            when (it.action) {
                ACTION_DOWN -> {
                    lastX = it.x
                    lastY = it.y
                    logger.info(
                            "event.x: ${it.x},event.y: ${it.y}," +
                            "rawX:${event.rawX},rawY:${event.rawY}," +
                                    "left: $left,top: $top," +
                            "translationX : $translationX, translationY: $translationY," +
                            "view.x: $x,view.y: $y")
                    logger.info("------down------")
                }
                ACTION_MOVE -> {
                    logger.info("-------rawY: ${it.rawY}, x: ${it.y}")
                    //如果没有重新绘制,view的top,left,right,bottom是不变的
                    if (it.rawX - it.x <0 || it.rawY - it.y <statusBarHeight) {
                        val p = parent as ViewGroup
                        p.scrollTo(0,0)
                        invalidate()

                    }else {
                        val offsetX = it.x - lastX
                        val offsetY = it.y - lastY

                        //方法一
//                    layout((left+offsetX).toInt(), (top+offsetY).toInt(),
//                            (right+offsetX).toInt(), (bottom+offsetY).toInt())
                        //方法二
//                    offsetLeftAndRight(offsetX.toInt())
//                    offsetTopAndBottom(offsetY.toInt())
                        //方法三
//                    val lp = layoutParams as ConstraintLayout.LayoutParams
//                    lp.leftMargin = offsetX.toInt() + left
//                    lp.topMargin = offsetY.toInt() + top
//                    layoutParams = lp
                        //方法四
                        val p = parent as ViewGroup
                        p.scrollBy((-offsetX).toInt(), (-offsetY).toInt())
                        invalidate()
                    }
                }
                ACTION_UP -> {
                    lastX = it.x
                    lastY = it.y
                }
                else -> {

                }
            }
        }
        return true
    }

}