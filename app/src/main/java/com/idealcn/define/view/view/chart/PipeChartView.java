package com.idealcn.define.view.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.idealcn.define.view.R;

import java.util.List;

/**
 * 日期: 2018/9/23 15:31
 * author: guoning
 * description:
 */
public class PipeChartView extends ChartView {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private List<? extends ChartData> chartDataList;


    public PipeChartView(Context context) {
        this(context,null);
    }

    public PipeChartView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public PipeChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        paint.setColor(ContextCompat.getColor(context, R.color.colorAccent));
        paint.setStyle(Paint.Style.FILL);
    }

    @Override
    public void setChartData(List<? extends ChartData> chartDataList) {
        this.chartDataList = chartDataList;
    }


    private RectF rectF ;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (rectF==null) {
            rectF = new RectF(getWidth() / 2, getPaddingTop(),
                    getWidth() - getPaddingRight(),
                    getHeight() / 2);
        }

        int width = getWidth() - getPaddingLeft() - getPaddingRight();
        int height = getHeight() - getPaddingTop() - getPaddingBottom();
        int radius = Math.min(width,height)/2;
        canvas.drawArc(rectF,
                -90f,
                30,
                true,
                paint);

    }
}
