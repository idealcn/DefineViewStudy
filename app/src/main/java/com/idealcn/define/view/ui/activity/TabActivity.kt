package com.idealcn.define.view.ui.activity

import android.graphics.Typeface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.idealcn.define.view.R
import com.idealcn.define.view.view.tabLayout.EqualSpaceTabLayout

class TabActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab)



        val favoriteTabView = findViewById<EqualSpaceTabLayout>(R.id.favoriteTabView)
        favoriteTabView.setMaxShowCount(4)
        favoriteTabView.setOnTabSelectedListener(object : EqualSpaceTabLayout.OnTabSelectedListener {
            override fun onTabSelected(position: Int) {
//                favoriteTabView.getTabAt(position).isSelected = true
            }

            override fun onTabUnSelected(position: Int) {
//                favoriteTabView.getTabAt(position).isSelected = false
            }

        })
        favoriteTabView.addTab(listOf("三个字", "文字比较长","文字刚刚好","我比较长"
            ,"三个字", "这个tab文字比较长" ,"22","这个tab文字比较长")) //, "两个字", "第五个tab"

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout.addTab(tabLayout.newTab().setText("11111"))
        tabLayout.addTab(tabLayout.newTab().setText("这个tab文字比较长"))
        tabLayout.addTab(tabLayout.newTab().setText("11111"))
        tabLayout.addTab(tabLayout.newTab().setText("这个tab文字比较长"))
        tabLayout.addTab(tabLayout.newTab().setText("22222222"))
        tabLayout.addTab(tabLayout.newTab().setText("这个tab文字比较长"))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
//                tab?.view?.isSelected = true

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
//                tab?.view?.isSelected = false
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                
            }

        })

    }



}