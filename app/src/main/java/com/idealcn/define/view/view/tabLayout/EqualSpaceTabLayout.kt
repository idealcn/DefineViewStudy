package com.idealcn.define.view.view.tabLayout

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.LinearInterpolator
import android.widget.HorizontalScrollView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.ViewCompat
import com.idealcn.define.view.R
import kotlin.math.max
import kotlin.math.min

/**
 * 问题：已解决
 * tab文字选中加粗后，如果不设置delta，宽度会被挤压，导致部分文字无法显示。
 * 如何解决：选中某个tab后，重新测量，重新计算
 *
 * 问题：
 * tab切换时，会偶现某些indicator先绘制再执行动画
 *
 * 特点：
 * 1：indicator长度固定
 * 2: tab文字等间距
 */


class EqualSpaceTabLayout : HorizontalScrollView {

    private val layoutInflater: LayoutInflater
    private var tabContainer: SlidingTabIndicator

    private val indicatorWidth = resources.getDimensionPixelSize(R.dimen.x60)
    private val indicatorHeight = resources.getDimensionPixelSize(R.dimen.x6)

    private val indicatorDrawable = GradientDrawable()

    private val tabIndicatorInterpolator: TabIndicatorInterpolator

    private var scrollAnimator: ValueAnimator? = null

    /**
     * indicator与tab在竖直方向上的间距
     */
    val verticalOffset = resources.getDimensionPixelOffset(R.dimen.x6)


    /**
     * 屏幕可见的tab最大数量
     */
    private var maxShowCount = 0


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        indicatorDrawable.setBounds(0, 0, indicatorWidth, indicatorHeight)
        tabContainer = SlidingTabIndicator(context)
        super.addView(tabContainer, 0, LayoutParams(WRAP_CONTENT, MATCH_PARENT))
        layoutInflater = LayoutInflater.from(context)
        tabIndicatorInterpolator = TabIndicatorInterpolator()
        tabIndicatorInterpolator.setIndicatorWidth(indicatorWidth)
    }


    inner class SlidingTabIndicator : LinearLayoutCompat {
        /**
         * indicator指示器的动画
         */
        private var indicatorAnimator: ValueAnimator? = null
        var selectedPosition = -1
        private var selectionOffset = 0f

        constructor(context: Context) : super(context) {
            orientation = LinearLayoutCompat.HORIZONTAL
            gravity = Gravity.CENTER_VERTICAL
            indicatorDrawable.orientation = GradientDrawable.Orientation.LEFT_RIGHT
            indicatorDrawable.cornerRadius = resources.getDimensionPixelSize(R.dimen.x4).toFloat()
            indicatorDrawable.shape = GradientDrawable.RECTANGLE
            indicatorDrawable.gradientType = GradientDrawable.LINEAR_GRADIENT
            indicatorDrawable.setSize(indicatorWidth, indicatorHeight)
            (indicatorDrawable.mutate() as GradientDrawable).colors = intArrayOf(
                resources.getColor(R.color.base_module_color_FF2892FF),
                resources.getColor(R.color.base_module_color_FF007EFF)
            )
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            // 父view预留给自己可用的宽度
            val pMeasuredWidth = MeasureSpec.getSize(widthMeasureSpec)
            val tabCount = getTabCount()
            if (pMeasuredWidth > 0) {

                var space = 0
                if (tabCount > maxShowCount) {
                    var w = 0
                    val rightOffset = 50

                    for (index in 0 until tabCount) {
                        if (index < maxShowCount) {
                            w += getTabAt(index).measuredWidth
                        }
                    }
                    space = (pMeasuredWidth - w - rightOffset) / (maxShowCount + 1)
                } else {
                    var w = 0
                    for (index in 0 until tabCount) {
                        if (index < maxShowCount) {
                            w += getTabAt(index).measuredWidth
                        }
                    }
                    space = (pMeasuredWidth - w) / (tabCount + 1)

                }


                for (index in 0 until tabCount) {
                    val child = getTabAt(index)
                    val params = child.layoutParams as MarginLayoutParams
                    params.leftMargin = space
                    // tab数量超过了maxShowCount，最后一个tab的右margin也要设置。
                    if (index == tabCount - 1) {
                        params.rightMargin = space
                    }
                    child.layoutParams = params

                }

                // tab中高度最大的那个
                var maxChildMeasuredHeight = 0
                // tab宽度累加之和
                var totalChildMeasuredWidth = 0
                for (index in 0 until tabCount) {
                    val child = getTabAt(index)
                    val childMeasuredWidth = child.measuredWidth
                    totalChildMeasuredWidth += childMeasuredWidth
                    maxChildMeasuredHeight = max(maxChildMeasuredHeight, child.measuredHeight)
                }

                // 重新设置自身的宽度,如果布局中高度设置的wrap_content,需要重新计算高度，避免indicator无法显示出来。
                if (MeasureSpec.getMode(heightMeasureSpec) != MeasureSpec.EXACTLY) {
                    println(this.javaClass.simpleName + "#onMeasure,重新设置自身的宽度")
                    val realSelfWidth = totalChildMeasuredWidth + (tabCount + 1) * space
                    val boundsHeight = indicatorDrawable.bounds.height()

                    setMeasuredDimension(
                        realSelfWidth,
                        /* 在父布局预留的高度和自己计算的高度之间选择最小值*/
                        min( /*父布局预留的高度*/ MeasureSpec.getSize(heightMeasureSpec),
                            /*自己计算的高度，乘以2是保证了tab在父布局中竖直居中*/
                            maxChildMeasuredHeight + (verticalOffset + boundsHeight) * 2
                        )
                    )
                }

            }

        }

        override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
            super.onLayout(changed, l, t, r, b)

            if (indicatorAnimator != null && indicatorAnimator!!.isRunning) {
                // It's possible that the tabs' layout is modified while the indicator is animating (ex. a
                // new tab is added, or a tab is removed in onTabSelected). This would change the target end
                // position of the indicator, since the tab widths are different. We need to modify the
                // animation's updateListener to pick up the new target positions.
//                updateOrRecreateIndicatorAnimation( /* recreateAnimation= */
//                    false, selectedPosition,300  /* duration= */-1
//                )
            } else {
                // If we've been laid out, update the indicator position
                jumpIndicatorToSelectedPosition()
            }
        }


        override fun dispatchDraw(canvas: Canvas) {

            val selectTab: TabView? =
                if (selectedPosition > -1 && selectedPosition < childCount) getChildAt(
                    selectedPosition
                ) as TabView else null
            val h = selectTab?.bottom ?: (height - indicatorHeight)
            val rect = indicatorDrawable.bounds
            indicatorDrawable.setBounds(
                rect.left,
                h + verticalOffset,
                rect.right,
                verticalOffset + h + indicatorHeight
            )
            indicatorDrawable.draw(canvas)
            super.dispatchDraw(canvas)
        }

        fun setIndicatorPositionFromTabPosition(position: Int, positionOffset: Float) {
            if (indicatorAnimator != null && indicatorAnimator!!.isRunning) {
                indicatorAnimator!!.cancel()
            }
            selectedPosition = position
            selectionOffset = positionOffset
            val selectedTitle = getChildAt(selectedPosition)
            val nextTitle = getChildAt(selectedPosition + 1)
            tweenIndicatorPosition(selectedTitle, nextTitle, selectionOffset)
        }

        fun animateIndicatorToPosition(position: Int, animationDuration: Int) {
            if (indicatorAnimator != null && indicatorAnimator!!.isRunning) {
                indicatorAnimator!!.cancel()
            }
//            selectedPosition = position
            updateOrRecreateIndicatorAnimation( /* recreateAnimation= */true,
                position,
                animationDuration
            )
        }

        private fun updateOrRecreateIndicatorAnimation(
            recreateAnimation: Boolean,
            position: Int,
            animationDuration: Int
        ) {
            //  indicator的动画效果无效的原因在于，这里的selectedPosition与position是相等的。只要不相等的时候，indicator动画才有效果。
            val currentView = getChildAt(selectedPosition)
            val targetView = getChildAt(position)
            if (targetView == null) {
                // If we don't have a view, just update the position now and return
                jumpIndicatorToSelectedPosition()
                return
            }
            // Create the update listener with the new target indicator positions. If we're not recreating
            // then animationStartLeft/Right will be the same as when the previous animator was created.

            // Create the update listener with the new target indicator positions. If we're not recreating
            // then animationStartLeft/Right will be the same as when the previous animator was created.
            val updateListener =
                AnimatorUpdateListener { valueAnimator ->
                    tweenIndicatorPosition(
                        currentView,
                        targetView,
                        valueAnimator.animatedFraction
                    )
                }

            if (recreateAnimation) {
                // Create & start a new indicatorAnimator.
                indicatorAnimator = ValueAnimator()
                val animator = indicatorAnimator!!
                animator.interpolator = LinearInterpolator()
                animator.duration = animationDuration.toLong()
                animator.setFloatValues(0f, 1f)
                animator.addUpdateListener(updateListener)
                animator.addListener(
                    object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animator: Animator) {
                            selectedPosition = position
                        }

                        override fun onAnimationEnd(animator: Animator) {
                            selectedPosition = position
                        }
                    })
                animator.start()
            } else {
                // Reuse the existing animator. Updating the listener only modifies the target positions.
                indicatorAnimator!!.removeAllUpdateListeners()
                indicatorAnimator!!.addUpdateListener(updateListener)
            }
        }


        private fun jumpIndicatorToSelectedPosition() {
            val currentView = getChildAt(selectedPosition)
            tabIndicatorInterpolator.setIndicatorBoundsForTab(
                this@EqualSpaceTabLayout, currentView, indicatorDrawable
            )
        }

        /**
         * Update the position of the indicator by tweening between the currently selected tab and the
         * destination tab.
         *
         *
         * This method is called for each frame when either animating the indicator between
         * destinations or driving an animation through gesture, such as with a viewpager.
         *
         * @param startTitle The tab which should be selected (as marked by the indicator), when
         * fraction is 0.0.
         * @param endTitle The tab which should be selected (as marked by the indicator), when fraction
         * is 1.0.
         * @param fraction A value between 0.0 and 1.0 that indicates how far between currentTitle and
         * endTitle the indicator should be drawn. e.g. If a viewpager attached to this TabLayout is
         * currently half way slid between page 0 and page 1, fraction will be 0.5.
         */
        private fun tweenIndicatorPosition(startTitle: View?, endTitle: View?, fraction: Float) {
            val hasVisibleTitle =
                startTitle != null && (startTitle.width > 0/* || startTitle.measuredWidth > 0*/)
            if (hasVisibleTitle) {
                tabIndicatorInterpolator.updateIndicatorForOffset(
                    this@EqualSpaceTabLayout, startTitle, endTitle, fraction, indicatorDrawable
                )
            } else {
                // Hide the indicator by setting the drawable's width to 0 and off screen.
                indicatorDrawable.setBounds(
                    -1,
                    indicatorDrawable.bounds.top,
                    -1,
                    indicatorDrawable.bounds.bottom
                )
            }
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }


    inner class TabView : LinearLayoutCompat {

        private val textView: AppCompatTextView

        constructor(context: Context) : super(context) {
            ViewCompat.setPaddingRelative(this, 0, 0, 0, 0)
            inflate(context, R.layout.layout_mine_favorite_tab, this)
            textView = findViewById(R.id.tvFavoriteTab)
        }

        fun setText(text: String): TabView {
            textView.text = text
            return this
        }

        fun setTextStyle(style: Int) {
            textView.setTypeface(Typeface.DEFAULT, style)
        }

        // TODO: 这两个方法还可以调整
        fun getContentWidth(): Int {
            return right - left
        }

        fun getContentHeight(): Int {
            return bottom - top
        }
    }


    fun getTabAt(index: Int): TabView {
        return tabContainer.getChildAt(index) as TabView
    }

    fun getTabCount() = tabContainer.childCount

    fun addTab(tabs: List<String>) {
        var tabView: TabView
        tabs.forEachIndexed { index, t ->
            tabView = TabView(context)
            tabView.setTextStyle(if (index == 0) Typeface.BOLD else Typeface.NORMAL)
            tabView.setText(tabs[index])
            tabView.tag = t
            tabView.setOnClickListener {
                setTabSelected(index)
            }
            tabContainer.addView(tabView)
        }
        setScrollPosition(0, 0f, updateSelectedText = false, updateIndicatorPosition = true)
    }


    fun setTabSelected(index: Int) {
        for (x in 0 until getTabCount()) {
            val tabView = getTabAt(x)
            if (x == index) {

                animateToTab(index)
                tabView.setTextStyle(Typeface.BOLD)
                mOnTabSelectedListener?.onTabSelected(x)
            } else {
                tabView.setTextStyle(Typeface.NORMAL)
                mOnTabSelectedListener?.onTabUnSelected(x)
            }
        }
    }

    private fun animateToTab(newPosition: Int) {
        val startScrollX = scrollX
        val targetScrollX: Int = calculateScrollXForTab(newPosition, 0f)

        if (startScrollX != targetScrollX) {
            ensureScrollAnimator()
            scrollAnimator!!.setIntValues(startScrollX, targetScrollX)
            scrollAnimator!!.start()
        }

        tabContainer.animateIndicatorToPosition(newPosition, 300)
    }

    private fun setScrollPosition(
        position: Int,
        positionOffset: Float,
        updateSelectedText: Boolean,
        updateIndicatorPosition: Boolean
    ) {
        val roundedPosition = Math.round(position + positionOffset)
        if (roundedPosition < 0 || roundedPosition >= tabContainer.getChildCount()) {
            return
        }

        // Set the indicator position, if enabled
        if (updateIndicatorPosition) {
            tabContainer.setIndicatorPositionFromTabPosition(position, positionOffset)
        }

        // Now update the scroll position, canceling any running animation
        if (scrollAnimator != null && scrollAnimator!!.isRunning) {
            scrollAnimator!!.cancel()
        }
        scrollTo(if (position < 0) 0 else calculateScrollXForTab(position, positionOffset), 0)

        // Update the 'selected state' view as we scroll, if enabled
        if (updateSelectedText) {
//            setSelectedTabView(roundedPosition)
        }
    }


    private fun ensureScrollAnimator() {
        if (scrollAnimator == null) {
            scrollAnimator = ValueAnimator()
            scrollAnimator!!.interpolator = LinearInterpolator()
            scrollAnimator!!.duration = 300
            scrollAnimator!!.addUpdateListener { animator ->
                scrollTo(
                    animator.animatedValue as Int,
                    0
                )
            }
        }
    }


    private fun calculateScrollXForTab(position: Int, positionOffset: Float): Int {
        val selectedChild: View = getTabAt(position)
        val nextChild: View? =
            if (position + 1 < getTabCount()) getTabAt(
                position + 1
            ) else null
        val selectedWidth = selectedChild.width
        val nextWidth = nextChild?.width ?: 0

        // base scroll amount: places center of tab in center of parent
        val scrollBase = selectedChild.left + selectedWidth / 2 - width / 2
        // offset amount: fraction of the distance between centers of tabs
        val scrollOffset = ((selectedWidth + nextWidth) * 0.5f * positionOffset).toInt()
        return if (ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_LTR) scrollBase + scrollOffset else scrollBase - scrollOffset
    }


    fun setMaxShowCount(max: Int) {
        this.maxShowCount = max
    }


    interface OnTabSelectedListener {
        fun onTabSelected(position: Int)
        fun onTabUnSelected(position: Int)
    }

    private var mOnTabSelectedListener: OnTabSelectedListener? = null

    fun setOnTabSelectedListener(listener: OnTabSelectedListener) {
        this.mOnTabSelectedListener = listener
    }

}