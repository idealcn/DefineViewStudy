package com.idealcn.define.view.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.idealcn.define.view.R
import com.idealcn.define.view.view.DragView2

class DragViewActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drag_view)
        val dragView2 = findViewById<DragView2>(R.id.dragView2)
        dragView2.setOnViewClickCallback(object : DragView2.OnViewClickCallback {
            override fun onClose(childView: View) {
                println("--------onClose-----------")
            }

            override fun onParentClick(parentView: View) {
                println("--------onParentClick-----------")
            }

        })
    }

}