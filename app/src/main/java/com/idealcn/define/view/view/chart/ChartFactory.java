package com.idealcn.define.view.view.chart;

import java.util.List;

/**
 * 日期: 2018/9/23 0:17
 * author: guoning
 * description:
 */
public class ChartFactory {

    private static ChartFactory factory = new ChartFactory();
    private ChartFactory(){}
    public static ChartFactory get(){
        return factory;
    }


    private ChartStyle style;
    private ChartData chart;
    private List<? extends ChartData> chartList;
    private ChartView chartView;

    public ChartFactory style(ChartStyle style){
        this.style = style;
        return this;
    }

    public ChartFactory chart(ChartData chart){
        this.chart = chart;
        return this;
    }

    public ChartFactory chartList(List<ChartData> chartList){
        this.chartList = chartList;
        return this;
    }

    public <T extends ChartView> ChartFactory view(T chartView){
        this.chartView = chartView;
        return  this;
    }

    public void  create(){

        chartView.setData(chartList);

    }
}
