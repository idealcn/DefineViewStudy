package com.idealcn.define.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.idealcn.define.view.R
import com.idealcn.define.view.view.MoveView

class ViewScrollActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_scroll)
        findViewById<MoveView>(R.id.moveView)
    }

}
