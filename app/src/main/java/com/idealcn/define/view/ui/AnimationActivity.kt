package com.idealcn.define.view.ui

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.Gravity
import android.view.KeyEvent
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.ImageView
import com.idealcn.define.view.R

/**
 * 日期: 2018/9/18 18:10
 * author: guoning
 * description:
 */
class AnimationActivity : AppCompatActivity() {

    var rippleDialog: AlertDialog? = null
    var pbDialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)
        findViewById<Button>(R.id.button)
            .setOnClickListener{

            //            val animation = AnimationUtils.loadAnimation(this@AnimationActivity, R.anim.anim_scale)
//            button.startAnimation(animation)
            val builder = AlertDialog.Builder(this@AnimationActivity)
            val contentView = layoutInflater.inflate(R.layout.dialog_progress, null)
            setContentView(contentView)
            val pb = contentView.findViewById<ImageView>(R.id.progressBar)
            val animation = RotateAnimation(-90f, 270f, Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f)
            animation.fillAfter = true
            animation.duration = 2000
            animation.repeatCount = Animation.INFINITE
            //插值器
            animation.interpolator = LinearInterpolator()
            pb.startAnimation(animation)

            builder.setCancelable(true)


            pbDialog =  builder.create()
            pbDialog!!.show()

            pbDialog!!.setOnDismissListener {it ->
                pb.clearAnimation()
            }
            pbDialog!!.setOnKeyListener { dialog, keyCode, event ->
                if (keyCode==KeyEvent.KEYCODE_BACK&&event.action == KeyEvent.ACTION_DOWN){
                    pbDialog!!.dismiss()
                }
                true
            }

            val window = pbDialog!!.window
            val attributes = window?.attributes
            attributes?.width = WindowManager.LayoutParams.WRAP_CONTENT
            attributes?.height = WindowManager.LayoutParams.WRAP_CONTENT
            attributes?.gravity = Gravity.CENTER
            window?.attributes = attributes


        }

        //水波纹效果
        findViewById<Button>(R.id.button2).setOnClickListener { it ->

            val builder = AlertDialog.Builder(this@AnimationActivity)
            builder.setCancelable(true)
            rippleDialog = builder.show()
            with(rippleDialog) {
                val contentView = layoutInflater.inflate(R.layout.dialog_ripple, null)
                val ivRipple = contentView.findViewById<ImageView>(R.id.ivRipple)

                val ivRipple1 = contentView.findViewById<ImageView>(R.id.ivRipple1)
                val ivRipple2 = contentView.findViewById<ImageView>(R.id.ivRipple2)
                val ivRipple3 = contentView.findViewById<ImageView>(R.id.ivRipple3)


                val animation = AnimationUtils.loadAnimation(this@AnimationActivity, R.anim.anim_scale_alpha_set)

                ivRipple1.startAnimation(animation)

                val animation2 = AnimationUtils.loadAnimation(this@AnimationActivity, R.anim.anim_scale_alpha_set)

                animation2.startOffset = 200
                ivRipple2.startAnimation(animation2)

                val animation3 = AnimationUtils.loadAnimation(this@AnimationActivity, R.anim.anim_scale_alpha_set)
                animation3.startOffset = 500
                ivRipple3.startAnimation(animation3)

                setContentView(contentView)


                val window = this!!.window
                val params = window?.attributes
                params?.width = WindowManager.LayoutParams.WRAP_CONTENT
                params?.height = WindowManager.LayoutParams.WRAP_CONTENT
                window?.attributes = params



                this.setOnDismissListener {
                    ivRipple2.clearAnimation()
                    ivRipple1.clearAnimation()
                    ivRipple3.clearAnimation()
                }
                this.setOnKeyListener { dialog, keyCode, event ->
                    if (keyCode == KeyEvent.KEYCODE_BACK && event!!.action == KeyEvent.ACTION_DOWN) {
                        rippleDialog!!.dismiss()
                    }
                    true
                }
            }


        }

        //缩放动画
//        val animation = ScaleAnimation(0.1f, 1.0f, 0.1f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
//        animation.fillAfter = true
//        animation.interpolator = BounceInterpolator()
//        animation.duration = 5000
//        imageView.startAnimation(animation)


    }


    override fun onDestroy() {
        super.onDestroy()
        if (rippleDialog != null) {
            rippleDialog!!.dismiss()
        }

        if (null != pbDialog) {
            pbDialog!!.dismiss()
        }
    }

//    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
//        if (keyCode==KeyEvent.KEYCODE_BACK && event!!.action == KeyEvent.ACTION_DOWN){
//            if (rippleDialog!=null){
//                rippleDialog!!.dismiss()
//
//            }
//            if (pbDialog!=null){
//                pbDialog!!.dismiss()
//
//            }
//        }
//        return super.onKeyDown(keyCode, event)
//    }
}