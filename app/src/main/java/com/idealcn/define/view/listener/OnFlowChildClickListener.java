package com.idealcn.define.view.listener;

import android.view.View;

/**
 * Created by ideal-gn on 2017/9/19.
 */

public interface OnFlowChildClickListener {
    void onChildClick(int index);
}
