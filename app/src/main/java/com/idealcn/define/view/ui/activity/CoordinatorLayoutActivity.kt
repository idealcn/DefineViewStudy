package com.idealcn.define.view.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.idealcn.define.view.R

class CoordinatorLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coordinator)

        val verticalDataList = listOf<String>(
                "上班","休息","生日","旅游","电竞","骑马","野渡","越野","沙漠","漂流","上班"
        )
        val verticalListView = findViewById<RecyclerView>(R.id.verticalListView)
        verticalListView.layoutManager =
            androidx.recyclerview.widget.GridLayoutManager(this, 5)
        verticalListView.adapter = VerticalAdapter(verticalDataList)




        val hDataList = listOf<String>(
                "星期一","星期二","星期三","星期四","星期五","星期六","星期日"
        )
        val horizontalListView = findViewById<RecyclerView>(R.id.horizontalListView)
        horizontalListView.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                false
            )
        horizontalListView.adapter = HorizontalAdapter(hDataList)


        findViewById<TextView>(R.id.dependency).setOnClickListener {
            ViewCompat.offsetTopAndBottom(it!!,5)
        }
    }



    inner class VerticalAdapter(var data: List<String>) : androidx.recyclerview.widget.RecyclerView.Adapter<VerticalAdapter.VerticalHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VerticalHolder {
           return VerticalHolder( LayoutInflater.from(this@CoordinatorLayoutActivity)
                   .inflate(android.R.layout.simple_list_item_1,null))
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: VerticalHolder, position: Int) {
            holder.itemView.findViewById<TextView>(android.R.id.text1).text = data[position]

        }


        inner class VerticalHolder(itemview :View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(itemview){

       }
    }


    inner class HorizontalAdapter(var data: List<String>) : androidx.recyclerview.widget.RecyclerView.Adapter<HorizontalAdapter.HorizontalHolder>(){

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorizontalHolder {
            return HorizontalHolder(
                    LayoutInflater.from(this@CoordinatorLayoutActivity)
                            .inflate(android.R.layout.simple_list_item_1,null))
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(holder: HorizontalHolder, position: Int) {
            holder.itemView.findViewById<TextView>(android.R.id.text1).text = data[position]
        }


        inner class HorizontalHolder(itemview :View) :
            androidx.recyclerview.widget.RecyclerView.ViewHolder(itemview){

        }
    }
}