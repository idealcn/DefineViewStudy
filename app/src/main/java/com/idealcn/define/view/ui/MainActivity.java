package com.idealcn.define.view.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.idealcn.define.view.listener.OnFlowChildClickListener;
import com.idealcn.define.view.listener.OnFragmentChangeListener;
import com.idealcn.define.view.R;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class MainActivity extends AppCompatActivity implements OnFragmentChangeListener,OnFlowChildClickListener {

    private static final String TAG = "main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MainFragment mainFragment = new MainFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.root,mainFragment).commit();


//        long totalRam = getTotalRam();
//        Runtime runtime = Runtime.getRuntime();
//        long totalMemory = runtime.totalMemory();
//        long freeMemory = runtime.freeMemory();
//        System.out.println("手机内存: "+
//                BigDecimal.valueOf(totalRam).divide(BigDecimal.valueOf(1024 * 1024),0, RoundingMode.UP).toPlainString()
//                +"GB,JVM分配的内存："+
//                BigDecimal.valueOf(totalMemory).divide(BigDecimal.valueOf(1024 * 1024),0, RoundingMode.UP).toPlainString()
//                + "MB,空闲内存："+
//                BigDecimal.valueOf(freeMemory).divide(BigDecimal.valueOf(1024 * 1024),0, RoundingMode.UP).toPlainString()
//                +"MB，虚拟机分配内存占比："+
//                BigDecimal.valueOf(totalMemory).divide(BigDecimal.valueOf(totalRam*1024),3,RoundingMode.UP).floatValue() * 100 +"%" );
    }

    public static long getTotalRam(){
        String path = "/proc/meminfo";
        String ramMemorySize = null;
        long totalRam = 0 ;
        try{
            FileReader fileReader = new FileReader(path);
            BufferedReader br = new BufferedReader(fileReader, 4096);
            ramMemorySize = br.readLine().split("\\s+")[1];
            br.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(ramMemorySize != null){
            try {
                totalRam = Long.parseLong(ramMemorySize);
                //(int)Math.ceil((Float.valueOf(Float.parseFloat(ramMemorySize) / (1024 * 1024)).doubleValue()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return totalRam; //+ "GB";
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "onWindowFocusChanged: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    public void onAttachFragment(androidx.fragment.app.Fragment fragment) {
        super.onAttachFragment(fragment);
            if (fragment instanceof MainFragment){
                ((MainFragment) fragment).setListener(this);
            }
    }


    @Override
    public void change(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onChildClick(int index) {
        switch (index){
            case 0:
//                listener.change("进度条");
                startActivity(new Intent(this, ProgressBarActivity.class));
                break;
//            case R.id.clock:
//                listener.change("时钟");
//                startActivity(new Intent(mainActivity, ClockActivity.class));
//                break;
//            case R.id.round_cake:
//                listener.change("饼状图");
//                startActivity(new Intent(mainActivity,RoundCakeActivity.class));
//                break;
//            case R.id.drag:
//                listener.change("拖拽");
//                startActivity(new Intent(mainActivity,DragActivity.class));
//                break;
//            case R.id.picSelector:
//                listener.change("图片选择器");
//                startActivity(new Intent(mainActivity,TableActivity.class));
//                break;
//            case R.id.flowLayout:
//                listener.change("流式布局");
//                startActivity(new Intent(mainActivity,FlowLayoutActivity.class));
//                break;
//            case R.id.padding:
//                listener.change("padding");
//                startActivity(new Intent(mainActivity,PaddingActivity.class));
//                break;
            default:break;
        }
    }


}
