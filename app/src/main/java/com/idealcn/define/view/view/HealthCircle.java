package com.idealcn.define.view.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author: guoning
 * @date: 2018/11/15 17:23
 * @description:
 */
public class HealthCircle extends View {


    private Paint outPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint innerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //画虚线
    private Paint dashPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    //弧线之间的间隔
    private int offset;

    //起始角度
    private float startAngle = 135f;
    //转过的角度
    private float sweepAngle = 221.4f;
    //总共需要转过的角度
    private float totalSweepAngle = 270f;
    //分数
    private int score = 0;

    private SweepGradient sweepGradient;

    private RectF dashRectF = new RectF();


    private int[] shaderColor =  {
//                            Color.rgb(99, 212, 155),
//                            Color.rgb(228, 179, 106),
//                            Color.rgb(238, 111, 68),
        Color.BLUE,Color.RED,Color.YELLOW
    };


    private int dp2Px(int dp) {
        return (int) (getResources().getDisplayMetrics().scaledDensity * (dp + 0.5f));
    }


    public HealthCircle(Context context) {
        super(context);
        init();
    }

    public HealthCircle(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HealthCircle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


        init();
    }

    private RectF rectF;
    private RectF rectF2;

    private void init() {

        offset = dp2Px(20);

        //最外层的弧线
        outPaint.setStyle(Paint.Style.STROKE);
        outPaint.setColor(Color.WHITE);
        outPaint.setStrokeWidth(dp2Px(1));

        //由外而内的第二层圆弧
        innerPaint.setStyle(Paint.Style.STROKE);
//        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(Color.parseColor("#63D49B"),PorterDuff.Mode.MULTIPLY);
//        innerPaint.setColorFilter(colorFilter);
        innerPaint.setStrokeWidth(dp2Px(20));


        //圆
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setColor(Color.WHITE);
        circlePaint.setStrokeWidth(dp2Px(1));

        //虚线
        dashPaint.setStyle(Paint.Style.STROKE);
        dashPaint.setColor(Color.WHITE);
        dashPaint.setStrokeWidth(dp2Px(1));


        //字体
        textPaint.setStyle(Paint.Style.STROKE);
        textPaint.setColor(Color.BLUE);
        textPaint.setStrokeWidth(dp2Px(2));
        textPaint.setTextSize(dp2Px(20));
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float CENTER = Math.min(getWidth(), getHeight()) / 2;
        canvas.drawColor(Color.parseColor("#041C42"));
        //画外层圆弧线
        if (rectF == null) {
            rectF = new RectF(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
        }
        canvas.drawArc(rectF,
                startAngle, sweepAngle, false, outPaint);


        //画第二层圆弧
        if (rectF2 == null) {
            rectF2 = new RectF(getPaddingLeft() + offset + outPaint.getStrokeWidth(),
                    getPaddingTop() + offset + outPaint.getStrokeWidth(),
                    getWidth() - getPaddingRight() - offset - outPaint.getStrokeWidth(),
                    getHeight() - getPaddingBottom() - offset - outPaint.getStrokeWidth());
        }

        if (sweepGradient == null) {
            //计算一个完整圆弧与360°的比值
            float start = startAngle / 360;
            float sweep = totalSweepAngle /360;
            //计算每一段圆弧所占的比值
            float div = sweep / shaderColor.length;
            sweepGradient = new SweepGradient(CENTER, CENTER,
                    shaderColor,
                    new float[]{
                           div/2 ,  3*div/2, 5*div/2
                            }
                                   );
        }
        innerPaint.setShader(sweepGradient);
        canvas.save();
        //旋转角度为负值,逆时针旋转,为正值,顺时针旋转
        canvas.rotate(startAngle,CENTER,CENTER);
        //画布已经旋转到0°,必须要先旋转到0°再去绘制
        canvas.drawArc(rectF2, 0, sweepAngle, false, innerPaint);
        canvas.rotate(-startAngle,CENTER,CENTER);
        canvas.restore();


        //画圆
        float radius = Math.min(getWidth(), getHeight()) / 2 - offset * 2 - innerPaint.getStrokeWidth() - outPaint.getStrokeWidth();
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, circlePaint);


        //画字体
        String text = String.valueOf(score);
        Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
        canvas.drawText(text, getWidth() / 2 - textPaint.measureText(text) / 2,
                getHeight() / 2 - (fontMetrics.descent + fontMetrics.ascent) / 2, textPaint);


        //画刻度
        //设定刻度在270°位置,所以要想从起始角度画刻度,应该先逆时针旋转到
        dashRectF.set(getWidth()/2 - dashPaint.getStrokeWidth()/2,
                    getHeight()/2 - radius + circlePaint.getStrokeWidth(),
                getWidth()/2 + dashPaint.getStrokeWidth()/2,
                getHeight()/2-radius + circlePaint.getStrokeWidth() + dp2Px(5));
        canvas.save();
        canvas.rotate(-(270-startAngle),CENTER,CENTER);
        //时钟有48个间隔
        for (int x = 0; x <= sweepAngle / (360/48); x++) {
            canvas.drawRect(dashRectF,dashPaint);
            canvas.rotate(360/48,CENTER,CENTER);
        }
        canvas.restore();
    }


    public HealthCircle setStartAngle(float startAngle){
        this.startAngle = startAngle;
        return this;
    }

    public HealthCircle setSweepAngle(float sweepAngle){
        this.sweepAngle = sweepAngle;
        return this;
    }

    public HealthCircle setScore(int score){
        this.score= score;
        return this;
    }

    public void start(){
        invalidate();
    }
}
