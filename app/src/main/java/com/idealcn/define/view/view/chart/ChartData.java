package com.idealcn.define.view.view.chart;

import java.io.Serializable;

/**
 * 日期: 2018/9/23 0:06
 * author: guoning
 * description:
 */
public class ChartData implements Serializable {

    public float height;
    public int color;
    public ChartStyle chartStyle;

    public ChartData(float height, int color, ChartStyle chartStyle) {
        this.height = height;
        this.color = color;
        this.chartStyle = chartStyle;
    }
}
