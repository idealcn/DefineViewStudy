package com.idealcn.define.view.behavior

import android.content.Context
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import java.util.logging.Logger

class HorizontalBehavior : androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior<TextView> {

    val logger = Logger.getLogger("HorizontalBehavior");

    constructor() : super()

    //这个构造函数用于在xml中定义时使用
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)


    override fun layoutDependsOn(parent: androidx.coordinatorlayout.widget.CoordinatorLayout, child: TextView, dependency: View): Boolean {
        return dependency is TextView
    }

    override fun onDependentViewChanged(parent: androidx.coordinatorlayout.widget.CoordinatorLayout, child: TextView, dependency: View): Boolean {
        dependency as TextView
        logger.info("dependency------translationY: "+dependency.translationY)
        logger.info("dependency------y: "+dependency.y)
        logger.info("dependency------top: "+dependency.top)

        logger.info("child------translationY: "+child.translationY)
        logger.info("child------y: "+child.y)
        logger.info("child------top: "+child.top)

        ViewCompat.offsetTopAndBottom(child, dependency.top.toInt() - child.top.toInt())
        return true
    }

    override fun onLayoutChild(parent: androidx.coordinatorlayout.widget.CoordinatorLayout, child: TextView, layoutDirection: Int): Boolean {


        return super.onLayoutChild(parent!!, child, layoutDirection)
    }
}