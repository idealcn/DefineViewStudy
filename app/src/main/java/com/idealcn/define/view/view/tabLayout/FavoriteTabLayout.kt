package com.idealcn.define.view.view.tabLayout

import android.content.Context
import android.util.AttributeSet
import android.view.ViewConfiguration
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.ViewCompat
import com.idealcn.define.view.R

class FavoriteTabLayout : LinearLayoutCompat {

    interface OnTabShowListener {
        fun <T> getTabName(t : T) : String
    }

    private var mOnTabShowListener : OnTabShowListener? = null

    fun setOnTabShowListener(listener: OnTabShowListener?) {
        this.mOnTabShowListener = listener
    }

    private val scaledTouchSlop : Int

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        scaledTouchSlop =  ViewConfiguration.get(context).scaledTouchSlop
        inflate(context,R.layout.layout_equal_space,this)
//        gravity = CENTER
//        orientation = LinearLayoutCompat.VERTICAL
//        val bottomMargin = resources.getDimensionPixelSize(R.dimen.x2)
//        val favoriteTabView =  FavoriteTabView(context)
//        val tabLayoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
//        tabLayoutParams.bottomMargin = bottomMargin
//        tabLayoutParams.weight = 1f
//        addView(favoriteTabView, tabLayoutParams)
//
//        val favoriteIndicator = FavoriteIndicator(context)
//        val topMargin = resources.getDimensionPixelSize(R.dimen.x4)
//        val indicatorParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
//        indicatorParams.topMargin = topMargin
//        addView(favoriteIndicator, indicatorParams)
//
//        val padding = (resources.displayMetrics.scaledDensity * 8).toInt()
//        setPadding(0, padding, 0, padding)
    }

     enum class TabMode {
        MODE_FIXED,MODE_SCROLL
    }
    private var tabMode = TabMode.MODE_FIXED
    fun setTabMode(mode : TabMode) {
        this.tabMode = mode
    }

    /**
     * 屏幕可见的tab最大数量
     */
    private var maxShowCount = -1

    fun setMaxShowCount(max : Int){
        this.maxShowCount = max
    }


    fun  addTab(tabs: List<String>) {
        val realRoot =  this.getChildAt(0) as LinearLayoutCompat
        val favoriteTabView = realRoot.getChildAt(0) as EqualSpaceTabLayout
        favoriteTabView.removeAllViews()
//        favoriteTabView.setTabMode(this.tabMode)
        favoriteTabView.setMaxShowCount(this.maxShowCount)
        favoriteTabView.addTab(tabs)
        val tabCount = favoriteTabView.childCount
        for (index in 0 until tabCount) {
            val tabChild = favoriteTabView.getChildAt(index)
            tabChild.setOnClickListener {
                favoriteTabView.setTabSelected(index)
                scrollIndicator(realRoot,index)
                for (y in 0 until tabCount) {
                    if (y == index) {
                        mOnTabSelectedListener?.onTabSelected(y)
                    } else {
                        mOnTabSelectedListener?.onTabUnSelected(y)
                    }
                }
            }
        }

    }

    private fun scrollIndicator(root: LinearLayoutCompat, index: Int) {
        val mFavoriteTabLayout =root. getChildAt(0) as EqualSpaceTabLayout
        val mFavoriteIndicator = root.getChildAt(1) as FavoriteIndicator
        ViewCompat.postOnAnimation(mFavoriteIndicator) {
            val childTab = mFavoriteTabLayout.getChildAt(index)
            val l = childTab.left + (childTab.measuredWidth / 2 - mFavoriteIndicator.width / 2)
            mFavoriteIndicator.animate().translationX((l - mFavoriteIndicator.left).toFloat())
                .setDuration(80).start()
        }
    }


    interface OnTabSelectedListener {
        fun onTabSelected(position: Int)
        fun onTabUnSelected(position: Int)
    }

    private var mOnTabSelectedListener: OnTabSelectedListener? = null

    fun setOnTabSelectedListener(listener: OnTabSelectedListener) {
        this.mOnTabSelectedListener = listener
    }

//    fun selectTab(position: Int) {
//        val favoriteTabView = getChildAt(0) as FavoriteTabView
//        val tabCount = favoriteTabView.childCount
//        if (tabCount == 0) return
//        favoriteTabView.setTabSelected(position)
//        scrollIndicator(realRoot, position)
//        for (index in 0 until tabCount) {
//            if (index == position) {
//                mOnTabSelectedListener?.onTabSelected(index)
//            } else {
//                mOnTabSelectedListener?.onTabUnSelected(index)
//            }
//        }
//    }


//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        // todo 如何根据子view设置自己的宽度
//        println("FavoriteTabLayout#measuredWidth: ${ getChildAt(0).measuredWidth}")
//       setMeasuredDimension( getChildAt(0).measuredWidth,
//          MeasureSpec.getSize(heightMeasureSpec))
//    }


    /*---------------------处理水平滑动---------------------------*/

//    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
//        when (ev.action) {
//            MotionEvent.ACTION_DOWN -> {
//                mStartX = ev.rawX.toInt()
//                mStartY = ev.rawY.toInt()
//            }
//            MotionEvent.ACTION_MOVE -> {
//                val diffY = ev.rawY - mStartY
//                val diffX = ev.rawX - mStartX
//                if (abs(diffX) > abs(diffY) && abs(diffX) > scaledTouchSlop) {
//                    println("水平滑动事件被拦截。。。")
//                    return true
//                }
//            }
//            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
//
//            }
//        }
//        return super.onInterceptTouchEvent(ev)
//    }
//
//
//      private val mScroller: Scroller = Scroller(context, AccelerateDecelerateInterpolator())
//
//      private var mStartX: Int = 0
//    private var mStartY : Int = 0
//      override fun onTouchEvent(event: MotionEvent): Boolean {
//          val action = event.action
//          println("当前事件：$action")
//          when (action) {
//              MotionEvent.ACTION_DOWN -> {
//                  mStartX = event.rawX.toInt()
//                  //                mStartX = (int) event.getX();
//                  if (!mScroller.isFinished) {
//                      mScroller.abortAnimation()
//                  }
//              }
//
//              MotionEvent.ACTION_MOVE -> {
//                  val mOldScrollX = scrollX
//                  val mEndX = event.rawX.toInt()
//                  var mNewScrollX = mOldScrollX + mStartX - mEndX
////                  if (mNewScrollX < 0) {
////                      mNewScrollX = 0
////                  }
////                  if (mNewScrollX > right - resources.displayMetrics.widthPixels /* todo 这里减去的是view的可视区域的宽度 */) {
////                      mNewScrollX =  right - resources.displayMetrics.widthPixels
////                  }
//                  println("ACTION_MOVE: mOldScrollX===>" + mOldScrollX + "mNewScrollX===>" + mNewScrollX)
//                  scrollTo(mNewScrollX, 0)
//                  //用Scroller实现滑动
//  //                mScroller.startScroll(mStartX,0,mNewScrollX-mOldScrollX,0);
//  //                invalidate();
//                  mStartX = mEndX
//              }
//              MotionEvent.ACTION_CANCEL,
//              MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> {
//                  val scrollX = scrollX
//                  println("ACTION_UP: scrollX===>$scrollX")
//                  val range = right - resources.displayMetrics.widthPixels
//                  if (scrollX > range / 2) {
//                      mScroller.startScroll(scrollX, 0, range - scrollX, 0)
//                      invalidate()
//                  } else {
//                      mScroller.startScroll(scrollX, 0, -scrollX, 0)
//                      invalidate()
//                  }
//                  mStartX = 0
//                  mStartY = 0
//              }
//          }
//          return true
//      }
//
//      override fun computeScroll() {
//          super.computeScroll()
//          if (mScroller.computeScrollOffset()) {
//              scrollTo(mScroller.currX, mScroller.currY)
//              invalidate()
//          }
//      }
    /*---------------------------------------------------------------*/
}